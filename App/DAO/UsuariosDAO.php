<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\DAO;

use App\Models\UsuarioModel;

/**
 * Description of UsuariosDAO
 *
 * @author henrique.guedes
 */
class UsuariosDAO extends Conexao {

    //put your code here

    function __construct() {
        parent::__construct();
    }

    public function getUserByEmail(string $usuario): UsuarioModel {
        $statement = $this->pdo
                ->prepare('SELECT id, nome, usuario, email, status, senha
                             FROM usuarios                        
                            WHERE email = :email');
        $statement->bindParam('email', $usuario);
        $statement->execute();
        $usuarios = $statement->fetch(\PDO::FETCH_ASSOC);
        //return count($usuario === 0 ? [] : $usuario);
        if (count($usuarios) === 0) {
            return NULL;
        } else {
            $user = new UsuarioModel();
            $user->setId($usuarios['id']);
            $user->setEmail($usuarios['email']);
            $user->setNome($usuarios['nome']);
            $user->setStatus($usuarios['status']);
            $user->setUsuario($usuarios['usuario']);
            $user->setSenha($usuarios['senha']);
            return $user;
        }
    }

}
