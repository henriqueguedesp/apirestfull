<?php

namespace App\Middlewares;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class JwtDateTimeMiddleware {

    public function __invoke(Request $request, Response $response, callable $next): Response {
        $token = $request->getAttribute('jwt');
        $expireDate = new \DateTime($token['expired_at']);
        $hoje = new \DateTime();
        if ($expireDate < $hoje) {
            return $response->withStatus(401);
        }
        $response = $next($request, $response);
        return $response;
    }

}
