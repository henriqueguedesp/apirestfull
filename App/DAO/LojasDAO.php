<?php

namespace App\DAO;

use App\Models\LojaModel;

class LojasDAO extends Conexao {

    //put your code here
    public function _construct() {
        parent::_construct();
    }

    public function getAllLojas(): array {
        $lojas = $this->pdo
                ->query('SELECT * FROM lojas ')
                ->fetchAll(\PDO::FETCH_ASSOC);
        return $lojas;
    }

    public function insertLoja(LojaModel $loja) {
        $statement = $this->pdo
                ->prepare('INSERT INTO lojas VALUES(
                        null,
                        :nome,
                        :telefone,
                        :endereco
                        );');
        $statement->execute([
            'nome' => $loja->getNome(),
            'endereco' => $loja->getEndereco(),
            'telefone' => $loja->getTelefone()
        ]);
    }
    public function updateLoja(LojaModel $loja) {
        $statement = $this->pdo
                ->prepare('UPDATE  lojas SET nome = :nome, endereco = :endereco, telefone = :telefone
                    WHERE id = :id');
        $statement->execute([
            ':id' => $loja->getId(),
            ':nome' => $loja->getNome(),
            ':endereco' => $loja->getEndereco(),
            ':telefone' => $loja->getTelefone()
        ]);
    }
    public function deleteLoja(int $id) {
        $statement = $this->pdo
                ->prepare('DELETE FROM lojas  WHERE id = :id');
        $statement->execute([
            ':id' => $id
        ]);
    }

}
