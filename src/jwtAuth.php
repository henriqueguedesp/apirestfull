<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace src;

use Tuupola\Middleware\JwtAuthentication;

function jwtAuth(): JwtAuthentication {
    return new JwtAuthentication([
        'secret' => getenv('JWT_SECURITY_KEY'),
        'attribute' => 'jwt'
    ]);
}
