<?php

/**
 * Description of index
 *
 * @author henrique.guedes
 */
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require_once 'vendor/autoload.php';

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$configuration = new \Slim\Container($configuration);

$mid01 = function (Request $request, Response $response, $next): Response {
    $response->getBody()->write("Dentro do middleare 01 <br>");
    $response = $next($request, $response);
    $response->getBody()->write("<br>Dentro do middleare 02");
    return $response;
};
$app = new \Slim\App($configuration);

$app->get('/', function ($request, $response, array $args) {
    return $response->getBody()->write('Bem-vindo API Valorem Agronegócios');
});
/*
  $app->get('/produtos[/{nome}]', function (Request $request, Response $response, array $args) {
  $limit = $request->getQueryParams()['limit'] ?? 10;
  $tipo = $request->getQueryParams()['tipo'] ?? 'todos';
  $nome = $args['nome'] ?? 'todos';
  return $response->getBody()->write("{$limit} produtos do tipo {$tipo} com o nome {$nome} banco de dados");
  });
 */
$app->post('/produto', function (Request $request, Response $response, array $args): Response {
    // $nome = $request->getQueryParams()['nome'] ?? 10;
    // $preco = $request->getQueryParams()['preco'] ?? 10;
    $data = $request->getParsedBody();
    $nome = $data['nome'] ?? '';
    $response->getBody()->write("Produto {$nome} (POST)");
    return $response;
})->add($mid01);

$app->put('/produto', function (Request $request, Response $response, array $args) {
    // $nome = $request->getQueryParams()['nome'] ?? 10;
    // $preco = $request->getQueryParams()['preco'] ?? 10;
    $data = $request->getParsedBody();
    $nome = $data['nome'] ?? '';
    return $response->getBody()->write("Produto {$nome} (PUT)");
});
$app->delete('/produto', function (Request $request, Response $response, array $args) {
    // $nome = $request->getQueryParams()['nome'] ?? 10;
    // $preco = $request->getQueryParams()['preco'] ?? 10;
    $data = $request->getParsedBody();
    $nome = $data['nome'] ?? '';
    return $response->getBody()->write("Produto {$nome} (DELETE)");
});

$app->run();
?>