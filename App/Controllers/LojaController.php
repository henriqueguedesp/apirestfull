<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\DAO\LojasDAO;
use App\Models\LojaModel;

class LojaController {

    public function getLojas(Request $request, Response $response, array $args): Response {
        $lojasDAO = new LojasDAO();
        $lojas = $lojasDAO->getAllLojas();
        $response = $response->withJson($lojas);
        return $response;
    }

    public function insertLoja(Request $request, Response $response, array $args): Response {
        $data = $request->getParsedBody();
        $lojasDAO = new LojasDAO();
        $loja = new LojaModel();
        $loja->setNome($data['nome']);
        $loja->setEndereco($data['endereco']);
        $loja->setTelefone($data['telefone']);
        $lojasDAO->insertLoja($loja);
        $response = $response->withJson([
            "mensagem" => "Loja inserida com sucesso! :)"
        ]);

        return $response;
    }

    public function updateLoja(Request $request, Response $response, array $args): Response {
        $data = $request->getParsedBody();
        $lojasDAO = new LojasDAO();
        $loja = new LojaModel();
        $loja->setId($data['id']);
        $loja->setNome($data['nome']);
        $loja->setEndereco($data['endereco']);
        $loja->setTelefone($data['telefone']);
        $lojasDAO->updateLoja($loja);
        $response = $response->withJson([
            "mensagem" => "Loja atualizada com sucesso! :)"
        ]);

        return $response;
    }

    public function deleteLoja(Request $request, Response $response, array $args): Response {
        $lojasDAO = new LojasDAO();
        $lojasDAO->deleteLoja($args['id']);
        $response = $response->withJson([
            "mensagem" => "Loja deletada com sucesso! :)"
        ]);


        return $response;
    }

}
