<?php

use function src\slimConfiguration;
use function src\basicAuth;
use function src\jwtAuth;
use App\Controllers\ProdutoController;
use App\Controllers\LojaController;
use App\Controllers\AuthController;
use Tuupola\Middleware\JwtAuthentication;
use App\Middlewares\JwtDateTimeMiddleware;

$app = new \Slim\App(slimConfiguration());

//$app->get('/', ProdutoController::class . ':getProdutos');

$app->post('/login', AuthController::class . ':login');
$app->get('/teste', function() {
            echo "oi";
        })
        ->add(new JwtDateTimeMiddleware())
        ->add(jwtAuth());

$app->post('/refresh_token', AuthController::class . ':refreshToken')
        ->add(jwtAuth());
 $app->get('/loja', LojaController::class . ':getLojas');

$app->group('', function () use ($app) {
            //$app->get('/loja', LojaController::class . ':getLojas');
            $app->post('/loja', LojaController::class . ':insertLoja');
            $app->put('/loja', LojaController::class . ':updateLoja');
            $app->delete('/loja/{id}', LojaController::class . ':deleteLoja');

            $app->get('/produto', ProdutoController::class . ':getProdutos');
            $app->post('/produto', ProdutoController::class . ':insertProduto');
            $app->put('/produto', ProdutoController::class . ':updateProduto');
            $app->delete('/produto', ProdutoController::class . ':deleteProduto');
        })->add(new JwtDateTimeMiddleware())
        ->add(jwtAuth());





$app->run();
