<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\DAO\UsuariosDAO;
use Firebase\JWT\JWT;
use DateTime;
use App\DAO\TokensDAO;
use App\Models\TokenModel;
use App\Models\UsuarioModel;

class AuthController {

    public function login(Request $request, Response $response, array $args): Response {
        $data = $request->getParsedBody();
        $usuariosDAO = new UsuariosDAO();
        $usuario = $usuariosDAO->getUserByEmail($data['email']);
        if (is_null($usuario->getEmail())) {
            return $response->withStatus(401);
        } else {
            if ($data['senha'] != $usuario->getSenha()) {
                return $response->withStatus(401);
            } else {
                $experiredAt = (new \DateTime())->modify('+2 days')->format('Y-m-d H:i:s');
                $tokenPayLoad = [
                    'sub' => $usuario->getId(),
                    'name' => $usuario->getNome(),
                    'email' => $usuario->getEmail(),
                    'expired_at' => $experiredAt
                ];
                $token = JWT::encode($tokenPayLoad, getenv('JWT_SECURITY_KEY'));
                $refreshTokenPayLoad = [
                    'email' => $usuario->getEmail(),
                    'ramdom' => uniqid()
                ];
                $refreshToken = JWT::encode($refreshTokenPayLoad, getenv('JWT_SECURITY_KEY'));
                $tokenModel = new TokenModel();
                $tokenModel->setToken($token);
                $tokenModel->setRefresh_token($refreshToken);
                $tokenModel->setExpired_at($experiredAt);
                $tokenModel->setUsuarios_id($usuario->getId());

                $tokenDAO = new TokensDAO();
                $tokenDAO->createToken($tokenModel);

                $response = $response->withJson([
                    'token' => $tokenModel->getToken(),
                    'refresh_token' => $tokenModel->getRefresh_token()
                ]);
            }
        }



        // $response = $response->withJson($usuario);
        return $response;
    }

    public function refreshToken(Request $request, Response $response, array $args): Response {
        $data = $request->getParsedBody();
        $refreshToken = $data['refresh_token'];
        $refreshTokenDecoded = JWT::decode(
                        $refreshToken, getenv('JWT_SECURITY_KEY'), ['HS256']);

        $tokenDAO = new TokensDAO();
        $existeRefreshToken = $tokenDAO->verifyRefreshToken($refreshToken);
        if (!$existeRefreshToken) {
            return $response->withStatus(401);
        } else {
            $usuarioDAO = new UsuariosDAO();

            $usuario = $usuarioDAO->getUserByEmail($refreshTokenDecoded->email);
              
            if (is_null($usuario)) {
                return $response->withStatus(401);
            } else {
                $experiredAt = (new \DateTime())->modify('+2 days')->format('Y-m-d H:i:s');
                $tokenPayLoad = [
                    'sub' => $usuario->getId(),
                    'name' => $usuario->getNome(),
                    'email' => $usuario->getEmail(),
                    'expired_at' => $experiredAt
                ];
                $token = JWT::encode($tokenPayLoad, getenv('JWT_SECURITY_KEY'));
                $refreshTokenPayLoad = [
                    'email' => $usuario->getEmail(),
                    'ramdom' => uniqid()
                ];
                $refreshToken = JWT::encode($refreshTokenPayLoad, getenv('JWT_SECURITY_KEY'));
                $tokenModel = new TokenModel();
                $tokenModel->setToken($token);
                $tokenModel->setRefresh_token($refreshToken);
                $tokenModel->setExpired_at($experiredAt);
                $tokenModel->setUsuarios_id($usuario->getId());
             
                $tokenDAO = new TokensDAO();
                $tokenDAO->createToken($tokenModel);

                $response = $response->withJson([
                    'token' => $tokenModel->getToken(),
                    'refresh_token' => $tokenModel->getRefresh_token()
                ]);
            }
        }
        return $response;
    }

}
