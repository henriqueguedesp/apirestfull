<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Conexao
 *
 * @author henrique.guedes
 */

namespace App\DAO;

abstract class Conexao {

    /**
     *
     * @var \PDO;
     */
    protected $pdo;

    public function __construct() {

        $host = getenv('DB_TESTE_HOST');
        $db = getenv('DB_TESTE_DBNAME');
        $user = getenv('DB_TESTE_USER');
        $pass = getenv('DB_TESTE_PASSWORD');
        $port = getenv('DB_TESTE_PORT');

        $dsn = "mysql:host={$host};dbname={$db};port={$port}";

        $this->pdo = new \PDO($dsn, $user, $pass);
        $this->pdo->setAttribute(
                \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION
        );
    }

}
