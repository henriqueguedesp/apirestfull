<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/**
 * Description of TokenModel
 *
 * @author henrique.guedes
 */
class TokenModel {
    //put your code here
    private $id;
    private $token;
    private $refresh_token;
    private $expired_at;
    private $usuarios_id;
    
    function __construct() {
        
    }
    
    function getId() {
        return $this->id;
    }

    function getToken() {
        return $this->token;
    }

    function getRefresh_token() {
        return $this->refresh_token;
    }

    function getExpired_at() {
        return $this->expired_at;
    }

    function getUsuarios_id() {
        return $this->usuarios_id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setToken($token) {
        $this->token = $token;
    }

    function setRefresh_token($refresh_token) {
        $this->refresh_token = $refresh_token;
    }

    function setExpired_at($expired_at) {
        $this->expired_at = $expired_at;
    }

    function setUsuarios_id($usuarios_id) {
        $this->usuarios_id = $usuarios_id;
    }



}
