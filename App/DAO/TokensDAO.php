<?php

namespace App\DAO;

use App\Models\TokenModel;

class TokensDAO extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function createToken(TokenModel $token) {
        $statement = $this->pdo
                ->prepare('INSERT INTO 
                        tokens (token, refresh_token, expired_at, usuarios_id)
                        VALUES 
                        (
                        :token, :refresh_token, :expired_at, :usuarios_id);');
        $statement->execute([
            'token' => $token->getToken(),
            'refresh_token' => $token->getRefresh_token(),
            'expired_at' => $token->getExpired_at(),
            'usuarios_id' => $token->getUsuarios_id()
        ]);
    }

    public function verifyRefreshToken(string $refreshToken): bool {
        $statement = $this->pdo
                ->prepare('SELECT 
id
FROM tokens
WHERE refresh_token = :refresh_token;');
        $statement->bindParam('refresh_token', $refreshToken);
        $statement->execute();
        $tokens = $statement->fetch(\PDO::FETCH_ASSOC);
        if (count($tokens) === 0) {
            return false;
        } else {
            return true;
        }
    }

}
